// Using require syntax
const usequery = window.executeESQuery;


// Using import syntax

let response;
async function eventHandler(id1, id2) {
  const data = {
    user1: id1,
    user2: id2,
  };

  await fetch("http://localhost:4000/room", {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data),
  })
    
}
async function runQuery() {
  const loader = document.getElementById("loader");
  loader.classList.add("visible");

  const issues = document.getElementById("issues");
  issues.classList.add("issues");

  try {
    const summary = document.getElementById("summary").value;
    const description = document.getElementById("description").value;
    response = await usequery(summary, description);
    loader.classList.remove("visible");

    // for id - 1 -------------------->
    const firstIssueId = document.getElementById("issue-id-1");
    firstIssueId.innerText = "Issue Id: " + response[0]._source.id;
    const firstSummary = document.getElementById("issue-summary-1");
    firstSummary.innerText = response[0]._source.summary;
    const firstDescription = document.getElementById("issue-description-1");
    firstDescription.innerText = response[0]._source.description;
    const commentsFirst = document.getElementById("issue-comments-1");
    for (let index = 0; index < response[0]._source.comments.length; index++) {
      const element = response[0]._source.comments[index];
      commentsFirst.innerHTML += `<li><div>${element.authorDisplayName}: ${element.body}</div></li><br/>`;
    }
    commentsFirst.innerHTML +=`<li><div>Saurabh Gupta: Contact ITOPS team with specific query in a compact format</div></li><br/>`

    // for id - 2 -------------------->
    const secondIssueId = document.getElementById("issue-id-2");
    secondIssueId.innerText = "Issue Id: " + response[1]._source.id;
    const secondSummary = document.getElementById("issue-summary-2");
    secondSummary.innerText = response[1]._source.summary;
    const secondDescription = document.getElementById("issue-description-2");
    secondDescription.innerText = response[1]._source.description;
    const commentsSecond = document.getElementById("issue-comments-2");
    for (let index = 0; index < response[1]._source.comments.length; index++) {
      const element = response[1]._source.comments[index];
      commentsSecond.innerHTML += `<li><div>${element.authorDisplayName}: ${element.body}</div></li><br/>`;
    }

    // for id - 3 -------------------->
    const thirdIssueId = document.getElementById("issue-id-3");
    thirdIssueId.innerText = "Issue Id: " + response[2]._source.id;
    const thirdSummary = document.getElementById("issue-summary-3");
    thirdSummary.innerText = response[2]._source.summary;
    const thirdDescription = document.getElementById("issue-description-3");
    thirdDescription.innerText = response[2]._source.description;
    const commentsThird = document.getElementById("issue-comments-3");
    for (let index = 0; index < response[2]._source.comments.length; index++) {
      const element = response[2]._source.comments[index];
      commentsThird.innerHTML += `<li><div>${element.authorDisplayName}: ${element.body}</div></li><br/>`;
    }

    // for id - 4 -------------------->
    const forthIssueId = document.getElementById("issue-id-4");
    forthIssueId.innerText = "Issue Id: " + response[3]._source.id;
    const forthSummary = document.getElementById("issue-summary-4");
    forthSummary.innerText = response[3]._source.summary;
    const forthDescription = document.getElementById("issue-description-4");
    forthDescription.innerText = response[3]._source.description;
    const commentsForth = document.getElementById("issue-comments-4");
    for (let index = 0; index < response[3]._source.comments.length; index++) {
      const element = response[3]._source.comments[index];
      commentsForth.innerHTML += `<li><div>${element.authorDisplayName}: ${element.body}</div></li><br/>`;
    }

    // for id - 5 -------------------->
    const fifthIssueId = document.getElementById("issue-id-5");
    fifthIssueId.innerText = "Issue Id: " + response[4]._source.id;
    const fifthSummary = document.getElementById("issue-summary-5");
    fifthSummary.innerText = response[4]._source.summary;
    const fifthDescription = document.getElementById("issue-description-5");
    fifthDescription.innerText = response[4]._source.description;
    const commentsFifth = document.getElementById("issue-comments-5");
    for (let index = 0; index < response[4]._source.comments.length; index++) {
      const element = response[4]._source.comments[index];
      commentsFifth.innerHTML += `<li><div>${element.authorDisplayName}: ${element.body}</div></li><br/>`;
    }

    issues.classList.remove("issues");

    if (response[5].Abstract.length > 0) {
      const abstract = document.getElementById("abstract");
      abstract.innerText = response[5].Abstract;
      const abstracturl = document.getElementById("abstract-url");
      abstracturl.innerHTML = response[5].AbstractURL;
      const topics = document.getElementById("topics");
      for (let index = 0; index < response[5].RelevantTopics.length; index++) {
        const element = response[5].RelevantTopics[index];
        topics.innerHTML += "<li>" + element + "</li><br/>";
      }
    }


    // // Handle the response and display the results
    // const resultsDiv = document.getElementById('results');
    // resultsDiv.innerHTML = JSON.stringify(response.hits);
  } catch (error) {
    console.error("Error:", error);
  }
}

const submitButton = document.getElementById("submitButton");
submitButton.addEventListener("click", runQuery);

const commentA = document.getElementById("issue-comments-1");
commentA.addEventListener("click", async function (e) {
  let targetName = e.target.innerText.split(":")[0];
  targetName = targetName.substring(0, targetName.length - 1).toLowerCase();
  let sourceName = document.getElementById("name").value;
  sourceName = sourceName.toLowerCase();
  let id1 = -1;
  let id2 = -1;
  for (let index = 0; index < response[6].data.length; index++) {
    const element = response[6].data[index].displayName.toLowerCase();
    if (element === targetName) {
      id1 = index + 1;
    } else if (element === sourceName) {
      id2 = index + 1;
    }
    if (id1 != -1 && id2 != -1) {
      break;
    }
  }
  await eventHandler(4, id2);
  window.location.replace("http://localhost:3000")
});
