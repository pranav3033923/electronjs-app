const { app, BrowserWindow, ipcMain,shell } = require("electron");
const path = require("path");
const { Client } = require("@elastic/elasticsearch");
const jaccardIndex = require("jaccard");
const natural = require("natural");
const redis = require("redis");
const { google } = require('googleapis');
const creds = require("./secrets.json");
let redisClient;

async function redisConnect() {
  redisClient = redis.createClient({
    password: creds.password,
    socket: {
      host: creds.host,
      port: creds.port,
    },
  });
  redisClient.on("error", (error) => console.error(`Error : ${error}`));

  await redisClient.connect();
}

const client = new Client({
  node: creds.esHost,
  auth: {
    username: "elastic",
    password: creds.esPassword,
  },
});

// Function to preprocess text using spaCy
async function preprocessText(text) {
  const tokenResp = natural.PorterStemmer.tokenizeAndStem(text);
  return tokenResp;
}

// Function to calculate Jaccard index
function calculateJaccardIndex(setA, setB) {
  return jaccardIndex.index(setA, setB);
}

async function cacheSimilarDocuments(index){
  const query = {
    index,
    body: {
      query: {
        match_all: {}, // Use "match_all" query to fetch all documents
      },
    },
    size: 1000, // Set a large size to retrieve all documents (modify if needed)
  };

  // Execute the search to retrieve all documents
  const searchResults = await client.search(query);

  // Get the retrieved documents
  const retrievedDocs = searchResults.body.hits.hits;
  return retrievedDocs;
}
// Function to search for similar documents
async function findSimilarDocuments(index, k, summary, description) {
  await redisConnect();
  // Preprocess the source document text
  const sourceText = `${summary} ${description}`;
  const sourceLemmas = await preprocessText(sourceText);
  const sourceTokens = new Set(sourceLemmas);


  // cache or retrieve concept for similar documents
  let resultsSimilarDocuments=[];
  try {
    const cacheResultsSimilarDocs = await redisClient.get("similarDocuments");
    if (cacheResultsSimilarDocs) {
      resultsSimilarDocuments = JSON.parse(cacheResultsSimilarDocs);
      console.log("Cache Detected of ES Query to fetch target Documents ------->");
    } else {
      resultsSimilarDocuments = await cacheSimilarDocuments(index);
      if (resultsSimilarDocuments.length === 0) {
        throw "API returned an empty array";
      }
      await redisClient.set("similarDocuments", JSON.stringify(resultsSimilarDocuments));
      console.log("Caching Done of ES Query to fetch target Documents ------->");
      
    }
    
  } catch (e) {
    console.error("An error occurred during caching in Redis ES - Either it is your first Query or not connected to the redis server",e);
  }


  // Calculate Jaccard index for each retrieved document
  for (const doc of resultsSimilarDocuments) {
    const docText = `${doc._source.summary} ${doc._source.description}`;
    const docLemmas = await preprocessText(docText);
    const docTokens = new Set(docLemmas);
    const jaccardIndex = calculateJaccardIndex(
      Array.from(sourceTokens),
      Array.from(docTokens)
    );
    doc._score = jaccardIndex;
  }

  // Sort the retrieved documents by Jaccard index
  resultsSimilarDocuments.sort((a, b) => b._score - a._score);

  // Return the top-k similar documents
  const similarDocs = resultsSimilarDocuments.slice(0, k);

  return similarDocs;
}

async function getQueryResults(query) {
  try {
    const customSearch = google.customsearch('v1');
    const apiKey = creds.apiKey;
    const searchQuery = query;
    const searchOptions = {
      cx: creds.cx,
      q: searchQuery
    };

    const res = await customSearch.cse.list({
      auth: apiKey,
      cx: searchOptions.cx,
      q: searchOptions.q
    });

    const searchResults = res.data.items;
    return searchResults;
  } catch (err) {
    console.error('Error executing search:', err);
    return undefined;
  }
  
}

async function getMembers() {
  try {
    const resp = await fetch(creds.GRAPH_API_URL, {
      headers: {
        Authorization: `Bearer ${creds.token}`,
      },
    });
    console.log("<-------Graph API response : successfully retrieved------->");
    const valueExtract = await resp.json();
    return valueExtract.value;
  } catch (e) {
    console.error("An error occurred while fetching data from Graph API");
    return [];
  }
}
async function getMembersCache() {
  let results;
  let isCached = false;
  try {
    const cacheResults = await redisClient.get("members");
    if (cacheResults) {
      isCached = true;
      results = JSON.parse(cacheResults);
    } else {
      results = await getMembers();
      if (results.length === 0) {
        throw "API returned an empty array";
      }
      await redisClient.set("members", JSON.stringify(results));
    }
    return {
      fromCache: isCached,
      data: results,
    };
  } catch (e) {
    console.error("An error occurred during caching in Redis", e);
    return [];
  }
}

function createWindow() {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, "preload.js"),
      referrerPolicy: "no-referrer-when-downgrade",
    },
  });

  
  win.loadFile("index.html");

}

app.whenReady().then(() => {
  createWindow();

  app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});
// Create a new BrowserWindow instance


// IPC handler for executing ES query in the main process
ipcMain.handle("executeESQuery", async (summary, description) => {
  try {
    let finalCollection = [];
    console.log("IPC MAIN EXECUTE......");

    const index = creds.index;
    const k = creds.K;

    console.log("<-----Fetching Similar Documents----->");
    let similarDocuments = await findSimilarDocuments(
      index,
      k,
      summary,
      description
    );
    console.log(
      "<-----Fetched Similar Documents successfully----->"
    );
    
    console.log("Connecting to Redis.........");
    await redisConnect();
    console.log("Connected to Redis.........");

    console.log("<----- Getting Members ------>");
    const membersDetails = await getMembersCache();
    console.log("<----- Fetched members ------>");


    
    console.log("<-----Query Running----->");
    const querySearch = await getQueryResults(summary + ": " + description);
    console.log("<-----Query Successfully Executed----->");

    let queryResults = {
      Abstract: querySearch === undefined ? "" : querySearch[0].title,
      AbstractURL: querySearch === undefined ? "" : querySearch[0].snippet,
      RelevantTopics: [],
    };
    if (querySearch!==undefined && querySearch !==null) {
      for (let index = 0; index < querySearch.length; index++) {
        const element = querySearch[index];
        const anchorURL=`<a href="${element.formattedUrl}" target='_blank'>${element.htmlSnippet}</a>`
        queryResults.RelevantTopics.push(
          anchorURL,
        );
      }
    }
    
    similarDocuments.push(queryResults);
    similarDocuments.push(membersDetails);

    console.log("<----- FINAL COLLECTION EVALUATED ------>");



    return similarDocuments;
  } catch (error) {
    console.error("Error occurred in IPC MAIN:", error);
    throw error;
  }
});
