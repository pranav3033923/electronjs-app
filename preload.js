const { contextBridge, ipcRenderer } = require("electron");

window.addEventListener('DOMContentLoaded', () => {
  const replaceText = (selector, text) => {
    const element = document.getElementById(selector)
    if (element) element.innerText = text
  }

  for (const type of ['chrome', 'node', 'electron']) {
    replaceText(`${type}-version`, process.versions[type])
  }
})

if(!window.executeESQuery){
contextBridge.exposeInMainWorld("executeESQuery", async (summary, description) => {
  try {
    console.log(`Executing`);
    const response = await ipcRenderer.invoke(
      "executeESQuery",
      summary,
      description
    );
    return response;
  } catch (error) {
    console.error("Error:", error);
    throw error;
  }
});
}
